package com.batyiry.coursmooc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

/**
 * Created by BatyIry on 16/03/2015.
 */
public class SettingsActivity extends Activity{

    TextView settings_t;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);

        settings_t = (TextView)findViewById(R.id.t_settings);
    }
 }

