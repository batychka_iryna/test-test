package com.batyiry.coursmooc;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by BatyIry on 16/03/2015.
 */
public class FavoritActivity extends Activity {

    TextView favorit_t;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.favorit_activity);

        favorit_t = (TextView)findViewById(R.id.t_favorit);
    }
}
