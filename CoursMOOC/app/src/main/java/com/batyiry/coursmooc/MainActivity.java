package com.batyiry.coursmooc;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    Button list_b, favotir_b, search_b, settings_b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        list_b = (Button)findViewById(R.id.b_list);
        favotir_b = (Button)findViewById(R.id.b_favorit);
        search_b = (Button)findViewById(R.id.b_search);
        settings_b = (Button)findViewById(R.id.b_settings);
        list_b.setOnClickListener(this);
        favotir_b.setOnClickListener(this);
        search_b.setOnClickListener(this);
        settings_b.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.b_list) {
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            startActivity(intent);
        }
        if(view.getId()==R.id.b_favorit) {
            Intent intent = new Intent(MainActivity.this, FavoritActivity.class);
            startActivity(intent);

        }
        if(view.getId()==R.id.b_search) {
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            startActivity(intent);

        }
        if(view.getId()==R.id.b_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);

        }



    }
}
