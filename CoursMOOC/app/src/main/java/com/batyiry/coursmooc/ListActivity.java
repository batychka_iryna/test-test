package com.batyiry.coursmooc;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by BatyIry on 16/03/2015.
 */
public class ListActivity extends Activity {

    TextView list_t;

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        list_t = (TextView)findViewById(R.id.t_list);
    }
}
