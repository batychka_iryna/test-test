package com.batyiry.mooc;



import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Kovb on 2/26/2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    static final String DB_NAME = "MOOC";
    static final int DB_VERSION = 19;
    //table de cours
    static final String TABLE_COURSES = "courses";
    static final String ID = "_id";
    static final String KEY = "key";
    static final String TITRE = "titre";
    static final String SUBTITRE = "subtitre";
    static final String DESCRIPTION = "description";
    static final String FULLDESCRIPTION = "fullDescription";
    static final String NIVEAU = "level";
    static final String DUREE = "duree";
    static final String MOOC = "mooc";
    static final String HOMEPAGE = "homePage";
    static final String LEARNING = "learning";
    static final String PROFS = "profs";
    static final String LANGUAGE = "language";
    static final String PREREQUIS = "prerequis";
    static final String ID_CATEGORY = "id_category";
    static final String NOMBRE_COURS_PAR_CATEGORY = "nombre_cours_par_category";

    //////////////////////////////////////////
    static final String TABLE_CATEGORY = "category";
    static final String CATEGORY_NAME = "category_name";
    /////////////////////////////////////////
    static final String TABLE_FAVORITE = "favorite";
    static final String ID_COURS_FAVORITE = "id_cours_favorite";
    /////////////////////////////////////////
    static final String TABLE_COURS_CATEGORY = "courses_categories";
    static final String ID_COURS = "id_course";
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql1 = "create table " +
                TABLE_COURSES + " (" +
                ID + " INTEGER primary key, " +
                KEY + " text, " +
                TITRE + " text, " +
                SUBTITRE + " text, " +
                DESCRIPTION + " text, " +
                FULLDESCRIPTION + " text, " +
                NIVEAU + " text, " +
                DUREE + " text, " +
                MOOC + " text, " +
                HOMEPAGE + " text, " +
                LEARNING + " text, " +
                PROFS + " text, " +
                LANGUAGE + " text, " +
                PREREQUIS + " text)";
        db.execSQL(sql1);
        Log.d("DB", "Database Courses created");

        String sql2 = "create table " +
                TABLE_CATEGORY + " (" +
                ID + " INTEGER primary key, " +
                CATEGORY_NAME + " text)";
        db.execSQL(sql2);
        Log.d("DB", "Database Categories created");

        String sql3 = "create table " +
                TABLE_COURS_CATEGORY + " (" +
                ID_COURS + " text, " +
                ID_CATEGORY + " INTEGER, "+
                "constraint Contstr1 foreign key (" + ID_COURS + ") references "+TABLE_COURSES+" ("+KEY+"), " +
                "constraint Contstr2 foreign key (" + ID_CATEGORY + ") references "+TABLE_CATEGORY+" ("+ID+"))";
        db.execSQL(sql3);
        Log.d("DB", "Database created");

        String sql4 = "create table if not exists " +
                TABLE_FAVORITE + " (" +
                ID_COURS + " text)";
        db.execSQL(sql4);
        Log.d("DB", "Database Favorite created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_COURSES);
        db.execSQL("drop table if exists " + TABLE_CATEGORY);
        db.execSQL("drop table if exists " + TABLE_COURS_CATEGORY);
        onCreate(db);
    }

    public Cursor listCourses(SQLiteDatabase db){  //Curser variable qui contient le resultat de requete SQL
        String sql = "select * from " + TABLE_COURSES
                + " order by " + TITRE + " asc";
        Cursor c = db.rawQuery(sql, null);
        Log.d("DB", "Liste des courses");
        return c;
    }

    public Cursor listCategories(SQLiteDatabase db){  //Curser variable qui contient le resultat de requete SQL
        /*String sq2 = "select * from " + TABLE_CATEGORY
                + " order by " + CATEGORY_NAME + " asc";*/
        String sql = "select "+
                "c." +CATEGORY_NAME + ", c." + ID + ", " +
                "csc." +ID_COURS + ", csc." + ID_CATEGORY +
                ", count("+ID_COURS + ") as " + NOMBRE_COURS_PAR_CATEGORY +
                " from " + TABLE_CATEGORY + " c, " + TABLE_COURS_CATEGORY + " csc" +
                " where c." + ID + "=csc." + ID_CATEGORY +
                " group by c." + CATEGORY_NAME +
                " order by c." + CATEGORY_NAME + " asc";

        Cursor c = db.rawQuery(sql, null);
        Log.d("DB", "Liste des categories");
        return c;
    }
}