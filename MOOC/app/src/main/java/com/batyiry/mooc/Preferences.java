package com.batyiry.mooc;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Created by Kovb on 3/27/2015.
 */
public class Preferences extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
    }
}
