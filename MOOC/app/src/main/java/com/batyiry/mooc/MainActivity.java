package com.batyiry.mooc;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.FragmentManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

    ViewPager pager;
    SimplePaperAdapter adapter;

    //Initialisation BD
    public static SQLiteDatabase db;
    public static DBHelper dbh;
    SharedPreferences shP;
    Color colScreen;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new SimplePaperAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);

        dbh = new DBHelper(this);
        db = dbh.getWritableDatabase();
        //Cursor c = dbh.listCourses(db);
        // Cursor c = dbh.listCategories(db);

        //Toast.makeText(this, "Chargement des donnees du Web", Toast.LENGTH_SHORT).show();
        //new DownloadWebTask().execute();

        //colScreen = new Color();
        //TextView textInfoPreferences = (TextView) findViewById(R.id);
        shP = PreferenceManager.getDefaultSharedPreferences(this);
        // clear preferences
        // shP.edit().clear().commit();

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            @Override
            public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                int pos=tab.getPosition();
                pager.setCurrentItem(pos);
                //Toast.makeText(getApplicationContext(),"TAB "+pos,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            }

            @Override
            public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
            }
        };
        for (int i = 0; i < adapter.getCount(); i++) {//indiquer le nom de l'ongle
            actionBar.addTab(actionBar.newTab().
                    setText(adapter.getPageTitle(i)).setTabListener(tabListener));
        }
        //indiquer le tab selectioner
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int position) {
                getSupportActionBar().setSelectedNavigationItem(position);
            }
        });
    }

    protected void OnResume(){
        boolean chBoxChecked = shP.getBoolean("checkbox_preference", false);
        String btn = shP.getString("button_refresh_preference", "REFRESH");
        //colScreen = Integer shP.getInt("list_colors", 255);
        //Color colorBackgraund = shP.getClass("list_colors", Color(#FFFFFF));
        //findViewById(R.id.textCategory).setBackgroundColor(R.class.colorBackgraund);

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem mi = menu.add(0,1,0, "Preferences");
        mi.setIntent(new Intent(this, Preferences.class));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private class SimplePaperAdapter extends FragmentPagerAdapter {

        public SimplePaperAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            PageFragment f = new PageFragment();
            Bundle args = new Bundle();
            args.putInt("id", i);
            f.setArguments(args);
            return f;
        }


        @Override
        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int i) {
            String lin = null;
            switch (i)
            {
                case 0:
                    lin =  "Categories";
                    break;
                case 1:
                    lin =  "Favorite";
                    break;
                case 2:
                    lin =  "Search";
                    break;
                /*case 3:
                    lin =  "Settings";

                    break;*/
            }
            return lin;
        }
    }

}
