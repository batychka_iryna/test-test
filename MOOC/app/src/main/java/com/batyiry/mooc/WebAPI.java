package com.batyiry.mooc;

import android.graphics.drawable.Drawable;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rania on 19/02/15.
 */
public class WebAPI {

    static ArrayList<String> idCours;
    static ArrayList<String> titres;
    static ArrayList<String> subtitles;
    static ArrayList<String> descriptions;
    static ArrayList<String> fullDescriptions;
    static ArrayList<String> niveax;
    static ArrayList<String> durees;
    static ArrayList<String> moocs;
    static ArrayList<String> homePages;
    static ArrayList<String> expectLearnings;
    static ArrayList<String> profs;
    static ArrayList<String> languages;
    static ArrayList <String> prerequis;
    //////////////categories qui contiennent les mot computer science
    static ArrayList <Integer> categoryComputerScience;
    //////////////
    static Map<Integer,String> categoriesMap;
    //static ArrayList<String> categoriesName;
    /////////////
    static Map <Integer, Session> sessionsMap;
    //static ArrayList<String> sessionLink;
    //static ArrayList<Long> sessionDate;
    //static ArrayList<String> sessionDuration;

    static Map <String, Integer> coursSessionMap;
    static Map <String, Integer> coursCategoryMap;



    private static void jsonUdacity(HttpEntity page){
        try{
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray courses = js.getJSONArray("courses");//("Lineups");
            Log.d("WEB", "UDACITY Nombre de courses: "+courses.length());
            for(int i =0;i<courses.length();i++){
                JSONObject item = courses.getJSONObject(i);
                String id = item.getString("key");
                String title = item.getString("title");
                String subtitle = item.getString("subtitle");
                String niveau = item.getString("level");
                String dureeDop = item.getString("expected_duration").concat(" ");
                String duree = dureeDop.concat(item.getString("expected_duration_unit"));
                String mooc = "UDACITY";
                String homePage = item.getString("homepage");
                String description = item.getString("short_summary");
                String fullDescription = item.getString("summary");
                String expectLearning = item.getString("expected_learning");
                String language = "en";
                String prerequis1 = item.getString("required_knowledge");
                JSONArray instructors = item.getJSONArray("instructors");
                StringBuffer sb = new StringBuffer();
                String profName = new String();
                for(int k =0;k<instructors.length();k++) {
                    JSONObject prof = instructors.getJSONObject(k);
                    sb.append(prof.getString("name"));
                    if(k<instructors.length()-1)
                        sb.append(", ");
                }
                profName = sb.toString();
                idCours.add(id);
                titres.add(title);
                descriptions.add(description);
                fullDescriptions.add(fullDescription);
                subtitles.add(subtitle);
                niveax.add(niveau);
                durees.add(duree);
                moocs.add(mooc);
                homePages.add(homePage);
                expectLearnings.add(expectLearning);
                profs.add(profName);
                languages.add(language);
                prerequis.add(prerequis1);
                //categor.add("Computer Science");
                coursCategoryMap.put(id, 99999);

                Log.d("Web", "Read cours UDACITY" + title + " #" + i +"   "+duree);
            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }
    }

    private static void jsonCoursera(HttpEntity page){
        try {
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray courses = js.getJSONArray("elements");
            Log.d("WEB", "COURSERA Nombre de courses: "+courses.length());

            for(int i=0;i<courses.length();i++){
                JSONObject item = courses.getJSONObject(i);
                String title = item.getString("name");
                String id = item.getString("id");
                String subtitle = "-";//item.getString("subtitle");

                int niveauDop = 3;
                if(item.has("targetAudience"))
                    niveauDop = Integer.parseInt(item.getString("targetAudience"));
                String niveau = new String();
                switch(niveauDop){
                    case 0:
                        niveau = "Basic undergraduates";break;
                    case 1:
                        niveau = "Advanced undergraduates or beginning graduates";break;
                    case 2:
                        niveau = "Advanced graduates";break;
                    case 3:
                        niveau = "Other";break;
                }
                String prerequis1;
                if(item.has("recommendedBackground"))
                    prerequis1 = item.getString("recommendedBackground");
                else prerequis1 = "-";

                String mooc = "COURSERA";

                String description;
                if(item.has("shortDescription"))
                    description = item.getString("shortDescription");
                else description = "-";

                String fullDescription;
                if(item.has("aboutTheCourse"))
                    fullDescription = item.getString("aboutTheCourse");
                else fullDescription = "-";

                String expectLearning = "-";//item.getString("expected_learning");
                String language = item.getString("language");
                String profName;
                if(item.has("instructor"))
                    profName = item.getString("instructor");
                else profName = "-";

                int idSession = coursSessionMap.get(id);
                String duree = sessionsMap.get(idSession).getDuree();
                String homePage = sessionsMap.get(idSession).getLink();
                /*int idCategory = coursCategoryMap.get(id);
                String category = categoriesMap.get(idCategory);
                String catLowCase = category.toLowerCase();
                if(!catLowCase.isEmpty())
                    if(catLowCase.contains("computer science"))
                        category = "Computer Science";
                */

                idCours.add(id);
                titres.add(title);
                descriptions.add(description);
                fullDescriptions.add(fullDescription);
                subtitles.add(subtitle);
                niveax.add(niveau);
                durees.add(duree);
                moocs.add(mooc);
                homePages.add(homePage);
                expectLearnings.add(expectLearning);
                profs.add(profName);
                languages.add(language);
                prerequis.add(prerequis1);
                //categor.add(category);
                Log.d("Web", "Read cours COURSERA" + title);

            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }

    }

    private static void jsonCourseraCoursesCategSession(HttpEntity page){
        try {
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray courses = js.getJSONArray("elements");//("Lineups");
            Log.d("WEB", "COURSERA Nombre de courses: "+courses.length());

            for(int i=0;i<courses.length();i++){
                JSONObject item = courses.getJSONObject(i);
                String id = item.getString("id");
                JSONObject link = item.getJSONObject("links");

                if(link.has("categories")) {
                    JSONArray categ = link.getJSONArray("categories");
                    int lenCateg = categ.length();
                    for(int j=0;j<lenCateg;j++){
                        int codeCateg = categ.getInt(j);
                        if(categoryComputerScience.contains(codeCateg))
                            coursCategoryMap.put(id, 99999);
                        else coursCategoryMap.put(id, codeCateg);

                    }
                }

                JSONArray sessions = link.getJSONArray("sessions");
                int codeSession = sessions.getInt(0);
                coursSessionMap.put(id, codeSession);
                //int lenSess = sessions.length();
                /*for(int j=0;j<lenSess;j++){
                    int codeSession = categories.getInt(ninja);
                }*/

                Log.d("Web", "Read cours COURSERA" + id);

            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }
    }
    private static void jsonCourseraSessions(HttpEntity page){
        try{
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray sessionsArr = js.getJSONArray("elements");
            Log.d("WEB", "COURSERA Nombre de sessions: "+sessionsArr.length());
            for(int i=0;i<sessionsArr.length();i++) {
                JSONObject item = sessionsArr.getJSONObject(i);
                int id = item.getInt("id");
                String link = item.getString("homeLink");

                int day = 0;
                if(item.has("startDay")) day = item.getInt("startDay");
                int month = 0;
                if(item.has("startMonth")) month = item.getInt("startMonth");
                int year = 0;
                if(item.has("startYear")) year = item.getInt("startYear");
                String duration = item.getString("durationString");
                Calendar cal=Calendar.getInstance();
                cal.set(year,month,day);
                long timestamp=cal.getTimeInMillis();

                Session sess = new Session(id, link,timestamp, duration);
                sessionsMap.put(id,sess);
                /*sessionID.add(id);
                sessionLink.add(link);
                sessionDuration.add(duration);*/

                //sessionDate.add(timestamp);

            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }
    }

    private static void jsonCourseraCategories(HttpEntity page){
        try{
            String contenu = EntityUtils.toString(page, HTTP.UTF_8);
            JSONObject js = new JSONObject(contenu);
            JSONArray categories = js.getJSONArray("elements");
            Log.d("WEB", "COURSERA Nombre de categories: "+categories.length());
            categoriesMap.put(99999, "Computer Science");
            for(int i=0;i<categories.length();i++) {
                JSONObject item = categories.getJSONObject(i);
                int id = item.getInt("id");
                String name = item.getString("name");
                String catLowCase = name.toLowerCase();
                if(!catLowCase.isEmpty()) {
                    if (catLowCase.contains("computer science"))
                        categoryComputerScience.add(id);
                    else
                        categoriesMap.put(id, name);
                }
            }
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        } catch (JSONException e) {
            Log.d("JSON ","Erreur: "+e.getMessage());
        }
    }
    public WebAPI(){

        idCours = new ArrayList<String>();
        titres = new ArrayList<String>();
        descriptions = new ArrayList<String>();
        subtitles = new ArrayList<String>();
        fullDescriptions = new ArrayList<String>();
        niveax = new ArrayList<String>();
        durees = new ArrayList<String>();
        moocs = new ArrayList<String>();
        homePages = new ArrayList<String>();
        expectLearnings = new ArrayList<String>();
        profs = new ArrayList<String>();
        languages = new ArrayList<String>();
        prerequis = new ArrayList<String>();
        categoryComputerScience = new ArrayList<Integer>();
        /////////
        categoriesMap = new HashMap();
        sessionsMap = new HashMap();
        coursSessionMap = new HashMap();
        coursCategoryMap = new HashMap();

        try {
            String url = "https://www.udacity.com/public-api/v0/courses";
            HttpEntity page = getHttp(url);
            jsonUdacity(page);
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        }

        try {
            String url = "https://api.coursera.org/api/catalog.v1/sessions?fields=durationString,startDay,startMonth,startYear";
            HttpEntity page = getHttp(url);
            jsonCourseraSessions(page);
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        }

        try {
            String url = "https://api.coursera.org/api/catalog.v1/categories";
            HttpEntity page = getHttp(url);
            jsonCourseraCategories(page);
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        }

        try {
            String url = "https://api.coursera.org/api/catalog.v1/courses?includes=categories,sessions";
            HttpEntity page = getHttp(url);
            jsonCourseraCoursesCategSession(page);
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        }

        try {
            String url = "https://api.coursera.org/api/catalog.v1/courses?fields=language,instructor" +
                    ",shortDescription,subtitleLanguagesCsv,aboutTheCourse,recommendedBackground," +
                    "targetAudience,courseSyllabus,courseFormat,instructor,estimatedClassWorkload";
            HttpEntity page = getHttp(url);
            jsonCoursera(page);
        } catch (IOException e) {
            Log.d("Web ","Erreur: "+e.getMessage());
        }

    }

    public HttpEntity getHttp(String url) throws ClientProtocolException, IOException {
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet http = new HttpGet(url);
        HttpResponse response = httpClient.execute(http);
        return response.getEntity();
    }

}