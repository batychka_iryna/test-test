package com.batyiry.mooc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Created by BatyIry on 19/03/2015.
 */
public class PageFragment extends Fragment
{
    private View rootView;
    private LayoutInflater inflaterFr;
    private ViewGroup containerFr;
    int id;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        id = args.getInt("id");
        //Toast.makeText(PageFragment.this.getActivity().getApplicationContext(), "onCreate "+Integer.toString(id), Toast.LENGTH_SHORT).show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = null;
        switch (id)
        {
            case 0:
                rootView = inflater.inflate(R.layout.simple_fragment_1, container, false);
                CreateCategoriesFragment(rootView);

                break;
            case 1:
                rootView = inflater.inflate(R.layout.simple_fragment_2, container, false);
                CreateFavoriteFragment(rootView);
                // function2
                break;
            case 2:
                rootView = inflater.inflate(R.layout.simple_fragment_3, container, false);
                CreateSearchFragment(rootView);
                break;
            /*case 3:
                rootView = inflater.inflate(R.layout.simple_fragment_4, container, false);
                break;*/
        }
        return rootView;
    }
    ///////////////////////Anna//////////////////////////////////
    private void CreateCategoriesFragment(View v1){
        Button btn;
        ListView listv;
        btn = (Button)v1.findViewById(R.id.btn_category_choisir);
        listv = (ListView)v1.findViewById(R.id.list_categories);
        Cursor c = MainActivity.dbh.listCategories(MainActivity.db);
        myAdapter adapter = new myAdapter(PageFragment.this.getActivity(), c);
        listv.setAdapter(adapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("ANNA", "On listener");
            }
        });
    }

    private class myAdapter extends CursorAdapter {
        LayoutInflater inflater;
        ArrayList<Integer> selectedCategories;

        public myAdapter(Context context, Cursor c){
            super(context, c, true);
            inflater = (LayoutInflater)PageFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            selectedCategories = new ArrayList<>();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if (v == null) {
                v = inflater.inflate(R.layout.rangee_choix, parent, false);
            }

            Cursor c = getCursor(); //recuperer cursor qui est cree dans constructeur
            c.moveToPosition(position);

            int idCateg = c.getInt(c.getColumnIndex((DBHelper.ID_CATEGORY)));
            String categoryName = c.getString(c.getColumnIndex(DBHelper.CATEGORY_NAME));
            String sumCours = c.getString(c.getColumnIndex(DBHelper.NOMBRE_COURS_PAR_CATEGORY));
            Button btnChoisirCategories = (Button)v.findViewById(R.id.btn_category_choisir);
            TextView categor = (TextView) v.findViewById(R.id.textCategory);
            TextView numbCours = (TextView) v.findViewById(R.id.textNumbCours);
            CheckBox chBox = (CheckBox) v.findViewById(R.id.chBoxChoixCaregory);

            categor.setText(categoryName);
            numbCours.setText(sumCours + ((Integer.parseInt(sumCours)>1)?" courses":" course"));

            boolean check = selectedCategories.contains((Integer) idCateg);
            chBox.setChecked(check);
            chBox.setTag(idCateg);

            chBox.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    CheckBox ch = (CheckBox)v;
                    Integer idCateg = (Integer) ch.getTag();
                    if (ch.isChecked()) {
                        if (!selectedCategories.contains(idCateg))
                            selectedCategories.add(idCateg);
                    }
                    else
                        if (selectedCategories.contains(idCateg))
                            selectedCategories.remove(idCateg);
                }

            });

            /*btnChoisirCategories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent("com.batyiry.mooc.CategoriesChoisies");
                    in.putExtra("Begin", selectedCategories);
                    //startActivity(i);
                }
            });*/
            /*chBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("CHECKBOX", "checked "+isChecked+ "" +buttonView.getTag());
                    Integer idCateg = (Integer) buttonView.getTag();
                    if(isChecked) {
                        if (!selectedCategories.contains(idCateg))
                            selectedCategories.add(idCateg);
                    }
                    else
                        if (selectedCategories.contains(idCateg))
                            selectedCategories.remove(idCateg);
                    }
            });*/


            return v;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return null;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

        }


    }

    //////////////////////Ira///////////////////////////////////
    private void CreateSearchFragment(View v){
        Button b;
        b = (Button)v.findViewById(R.id.b_search);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("IRA", "On listener");
            }
        });
    }
    private void CreateFavoriteFragment(View v_favorite){
        ListView lv_favorite;
        lv_favorite = (ListView)v_favorite.findViewById(R.id.lvFavorite);
        Cursor c = MainActivity.dbh.listCategories(MainActivity.db);
        myAdapter adapter = new myAdapter(PageFragment.this.getActivity(), c);
        lv_favorite.setAdapter(adapter);
        }

    private class adapterFavorite extends CursorAdapter {
        LayoutInflater inflater;

        public adapterFavorite(Context context, Cursor c){
            super(context, c, true);
            inflater = (LayoutInflater)PageFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;

            if(v==null){
                v = inflater.inflate(android.R.layout.simple_list_item_2, parent, false);
            }
            Cursor c = getCursor(); //recuperer cursor qui est cree dans constructeur
            c.moveToPosition(position);
            String cat = c.getString(c.getColumnIndex(DBHelper.ID));
            String MOOC = c.getString(c.getColumnIndex(DBHelper.CATEGORY_NAME));

            TextView titre = (TextView)v.findViewById(android.R.id.text1);
            TextView duree = (TextView)v.findViewById(android.R.id.text2);
            titre.setText(cat);
            duree.setText(MOOC);

            return v;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            return null;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

        }

    }
    ///////////////////Natasha//////////////////////////////////
    //////////////////Felix////////////////////////////////////
}
