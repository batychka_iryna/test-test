package com.batyiry.mooc;

/**
 * Created by Kovb on 3/19/2015.
 */
public class Session {
    int id;
    String sessionLink;
    Long sessionDate;
    String sessionDuration;

    public Session(){
        this.id = 0;
        this.sessionLink = "";
        this.sessionDate = new Long(0);
        this.sessionDuration = "";
    }

    public Session(int id, String sessionLink, Long sessionDate, String sessionDuration){
        this.id = id;
        this.sessionLink = sessionLink;
        this.sessionDate = sessionDate;
        this.sessionDuration = sessionDuration;
    }

    public String getDuree(){
        return this.sessionDuration;
    }

    public String getLink(){
        return this.sessionLink;
    }

    public Long getDate(){
        return this.sessionDate;
    }

}
