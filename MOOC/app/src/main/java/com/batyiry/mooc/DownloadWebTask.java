package com.batyiry.mooc;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Map;

/**
 * Created by BatyIry on 21/03/2015.
 */
public class DownloadWebTask extends AsyncTask<Void, Void, Void> {//type de reture 3 methode, dernier doinbackgraund

    @Override
    protected Void doInBackground(Void... params) {

        WebAPI web = new WebAPI();
        ContentValues cv = new ContentValues();

        long k1 = MainActivity.db.delete(DBHelper.TABLE_COURSES, null, null);
        if(k1 == 0)
            Log.e("DB", "ERROR  Table Courses items deleted");
        Log.d("DB", "Table Courses items are deleted");
        for(int i=0; i<web.titres.size(); i++){
            String key = web.idCours.get(i);
            String t = web.titres.get(i);
            String d = web.descriptions.get(i);
            String st = web.subtitles.get(i);
            String fd = web.fullDescriptions.get(i);
            String n = web.niveax.get(i);
            String dr = web.durees.get(i);
            String m = web.moocs.get(i);
            String h = web.homePages.get(i);
            String el = web.expectLearnings.get(i);
            String p = web.profs.get(i);
            String lg = web.languages.get(i);
            String prq = web.prerequis.get(i);
            cv.clear();
            cv.put(DBHelper.ID, i);
            cv.put(DBHelper.KEY, key);
            cv.put(DBHelper.TITRE, t);
            cv.put(DBHelper.DESCRIPTION, d);
            cv.put(DBHelper.SUBTITRE, st);
            cv.put(DBHelper.FULLDESCRIPTION, fd);
            cv.put(DBHelper.NIVEAU, n);
            cv.put(DBHelper.DUREE, dr);
            cv.put(DBHelper.MOOC, m);
            cv.put(DBHelper.HOMEPAGE, h);
            cv.put(DBHelper.LEARNING, el);
            cv.put(DBHelper.PROFS, p);
            cv.put(DBHelper.LANGUAGE, lg);
            cv.put(DBHelper.PREREQUIS, prq);
            k1 = MainActivity.db.insert(DBHelper.TABLE_COURSES, null, cv);
            if(k1 == 0)
                Log.e("DB", "Table courses: Item existe deja");
        }
        Log.d("MainActivity:", "Table Courses: elements inserted");

        long k2 = MainActivity.db.delete(DBHelper.TABLE_CATEGORY, null, null);
        if(k2 == 0)
            Log.e("DB", "ERROR  Table categories: Items deleted");
        Log.d("DB", "Table categories: Items are deleted");
        for(Map.Entry<Integer,String> entry : web.categoriesMap.entrySet()) {
            int k = entry.getKey();
            String v = entry.getValue();
            cv.clear();
            cv.put(DBHelper.ID, k);
            cv.put(DBHelper.CATEGORY_NAME, v);
            k2 = MainActivity.db.insert(DBHelper.TABLE_CATEGORY, null, cv);
            if(k2 == 0)
                Log.e("DB", "Table categories: Item existe deja");
        }
        Log.d("MainActivity:", "Table categories: elements inserted");

        long k3 = MainActivity.db.delete(DBHelper.TABLE_COURS_CATEGORY, null, null);
        if(k3 == 0)
            Log.e("DB", "ERROR Table CoursesCategories: Items deleted");
        Log.d("DB", "CoursesCategories: Items are deleted");
        for(Map.Entry<String, Integer> entry : web.coursCategoryMap.entrySet()) {
            String k = entry.getKey();
            int v = entry.getValue();
            cv.clear();
            cv.put(DBHelper.ID_COURS, k);
            cv.put(DBHelper.ID_CATEGORY, v);
            k3 = MainActivity.db.insert(DBHelper.TABLE_COURS_CATEGORY, null, cv);
            if(k3 == 0)
                Log.e("DB", "Table CoursesCategories: Item existe deja");
        }
        Log.d("MainActivity:", "Table CoursesCategories: elements inserted");
        //favorite
       /* long k4 = MainActivity.db.delete(DBHelper.TABLE_FAVORITE, null, null);
        if(k4 == 0)
            Log.e("DB", "ERROR  Table FAVORITE Items deleted");
        Log.d("DB", "Table FAVORITE : Items are deleted");
        for(Map.Entry<Integer,String> entry : web.categoriesMap.entrySet()) {
            int k = entry.getKey();
            String v = entry.getValue();
            cv.clear();
            cv.put(DBHelper.ID, k);
            k4 = MainActivity.db.insert(DBHelper.TABLE_FAVORITE, null, cv);
            if(k4 == 0)
                Log.e("DB", "Table FAVORITE: Item existe deja");
        }
        Log.d("MainActivity:", "Table FAVORITE: elements inserted");*/

        return null;
    }

    @Override
    protected void onPreExecute() {
        // btn.setEnabled(false);
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Void v) {
        //  btn.setEnabled(true);
        //Cursor c = dbh.listCourses(db);
        //Cursor c = dbh.listCategories(db);
        //adapter.changeCursor(c);
        Log.d("MainActivity:", "BD est cree");

    }
}
