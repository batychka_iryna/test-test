def ADDS1B3(a,b):
    if(a=='0' and  b=='0'):
        return('0','0')
    if(a=='0' and  b=='1'):
        return('1','0')
    if(a=='0' and  b=='2'):
        return('2','0')
    if(a=='1' and  b=='0'):
        return('1','0')
    if(a=='1' and  b=='1'):
        return('2','0')
    if(a=='1' and  b=='2'):
        return('0','1')
    if(a=='2' and  b=='0'):
        return('2','0')
    if(a=='2' and  b=='1'):
        return('0','1')
    if(a=='2' and  b=='2'):
        return('1','1')
    raise ValueError('unknown digits in ADDS1B3')
