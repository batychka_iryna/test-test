def ADDS1B5(a,b):
    if(a=='0' and  b=='0'):
        return('0','0')
    if(a=='0' and  b=='1'):
        return('1','0')
    if(a=='0' and  b=='2'):
        return('2','0')
    if(a=='0' and  b=='3'):
        return('3','0')
    if(a=='0' and  b=='4'):
        return('4','0')
    if(a=='1' and  b=='0'):
        return('1','0')
    if(a=='1' and  b=='1'):
        return('2','0')
    if(a=='1' and  b=='2'):
        return('3','0')
    if(a=='1' and  b=='3'):
        return('4','0')
    if(a=='1' and  b=='4'):
        return('0','1')
    if(a=='2' and  b=='0'):
        return('2','0')
    if(a=='2' and  b=='1'):
        return('3','0')
    if(a=='2' and  b=='2'):
        return('4','0')
    if(a=='2' and  b=='3'):
        return('0','1')
    if(a=='2' and  b=='4'):
        return('1','1')
    if(a=='3' and  b=='0'):
        return('3','0')
    if(a=='3' and  b=='1'):
        return('4','0')
    if(a=='3' and  b=='2'):
        return('0','1')
    if(a=='3' and  b=='3'):
        return('1','1')
    if(a=='3' and  b=='4'):
        return('2','1')
    if(a=='4' and  b=='0'):
        return('4','0')
    if(a=='4' and  b=='1'):
        return('0','1')
    if(a=='4' and  b=='2'):
        return('1','1')
    if(a=='4' and  b=='3'):
        return('2','1')
    if(a=='4' and  b=='4'):
        return('3','1')
    raise ValueError('unknown digits in ADDS1B5')
