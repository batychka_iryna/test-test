from ADDS1 import ADDS1
from ADDS2 import ADDS2
from ADDS3 import ADDS3
from ADDS1B2 import ADDS1B2
from ADDS1B3 import ADDS1B3
from ADDS1B4 import ADDS1B4
from ADDS1B5 import ADDS1B5
from ADDS1B6 import ADDS1B6
from ADDS1B7 import ADDS1B7
from ADDS1B8 import ADDS1B8
from ADDS1B9 import ADDS1B9
from ADDS1B10 import ADDS1B10
from ADDS1B11 import ADDS1B11
from ADDS1B12 import ADDS1B12
from ADDS1B13 import ADDS1B13
from ADDS1B14 import ADDS1B14
from ADDS1B15 import ADDS1B15
from ADDS1B16 import ADDS1B16
#
from ADDS2B2 import ADDS2B2
from ADDS2B3 import ADDS2B3
from ADDS2B4 import ADDS2B4
from ADDS2B5 import ADDS2B5
from ADDS2B6 import ADDS2B6
from ADDS2B7 import ADDS2B7
from ADDS2B8 import ADDS2B8
from ADDS2B9 import ADDS2B9
from ADDS2B10 import ADDS2B10
from ADDS2B11 import ADDS2B11
from ADDS2B12 import ADDS2B12
from ADDS2B13 import ADDS2B13
from ADDS2B14 import ADDS2B14
from ADDS2B15 import ADDS2B15
from ADDS2B16 import ADDS2B16
#
from ADDS3B2 import ADDS3B2
from ADDS3B3 import ADDS3B3
from ADDS3B4 import ADDS3B4
from ADDS3B5 import ADDS3B5
from ADDS3B6 import ADDS3B6
from ADDS3B7 import ADDS3B7
from ADDS3B8 import ADDS3B8
from ADDS3B9 import ADDS3B9
from ADDS3B10 import ADDS3B10
from ADDS3B11 import ADDS3B11
from ADDS3B12 import ADDS3B12
from ADDS3B13 import ADDS3B13
from ADDS3B14 import ADDS3B14
from ADDS3B15 import ADDS3B15
from ADDS3B16 import ADDS3B16

def tfuncVar(ADD,ch,base, alg):
    for a in ch:
        for b in ch:
            try:
                r = ADD(str(a),str(b))
                print ("ADDS{3}B{4}(\'{0}\',\'{1}\')=={2}".format(a,b,r,alg,base))
            except ValueError as e:
                print ("a = {0}, b = {1}".format(a,b),e)
                break
                    
for a in range (0,10):
    for b in range (0,10):
        r = ADDS1(str(a),str(b))
        print ("ADDS1(\'{0}\',\'{1}\')=={2}".format(a,b,r))
print('******')

for a in range (0,10):
    for b in range (0,10):
        r = ADDS2(str(a),str(b))
        print ("ADDS2(\'{0}\',\'{1}\')=={2}".format(a,b,r))
print('******')

for a in range (0,10):
    for b in range (0,10):
        r = ADDS3(str(a),str(b))
        print ("ADDS3(\'{0}\',\'{1}\')=={2}".format(a,b,r))
print('******')

ch="01"
tfuncVar(ADDS1B2,ch,2, 1)
print('*')
tfuncVar(ADDS2B2,ch,2, 2)
print('*')
tfuncVar(ADDS3B2,ch,2, 3)
print('******')
ch="012"
tfuncVar(ADDS1B3,ch,3, 1)
print('*')
tfuncVar(ADDS2B3,ch,3, 2)
print('*')
tfuncVar(ADDS3B3,ch,3, 3)
print('******')
ch="0123"
tfuncVar(ADDS1B4,ch,4, 1)
print('*')
tfuncVar(ADDS2B4,ch,4, 2)
print('*')
tfuncVar(ADDS3B4,ch,4, 3)
print('******')
ch="01234"
tfuncVar(ADDS1B5,ch,5, 1)
print('*')
tfuncVar(ADDS2B5,ch,5, 2)
print('*')
tfuncVar(ADDS3B5,ch,5, 3)
print('******')
ch="012345"
tfuncVar(ADDS1B6,ch,6, 1)
print('*')
tfuncVar(ADDS2B6,ch,6, 2)
print('*')
tfuncVar(ADDS3B6,ch,6, 3)
print('******')
ch="0123456"
tfuncVar(ADDS1B7,ch,7, 1)
print('*')
tfuncVar(ADDS2B7,ch,7, 2)
print('*')
tfuncVar(ADDS3B7,ch,7, 3)
print('******')
ch="01234567"
tfuncVar(ADDS1B8,ch,8, 1)
print('*')
tfuncVar(ADDS2B8,ch,8, 2)
print('*')
tfuncVar(ADDS3B8,ch,8, 3)
print('******')
ch="012345678"
tfuncVar(ADDS1B9,ch,9, 1)
print('*')
tfuncVar(ADDS2B9,ch,9, 2)
print('*')
tfuncVar(ADDS3B9,ch,9, 3)
print('******')
ch="0123456789"
tfuncVar(ADDS1B10,ch,10, 1)
print('*')
tfuncVar(ADDS2B10,ch,10, 2)
print('*')
tfuncVar(ADDS3B10,ch,10, 3)
print('******')
ch="0123456789A"
tfuncVar(ADDS1B11,ch,11, 1)
print('*')
tfuncVar(ADDS2B11,ch,11, 2)
print('*')
tfuncVar(ADDS3B11,ch,11, 3)
print('******')
ch="0123456789AB"
tfuncVar(ADDS1B12,ch,12, 1)
print('*')
tfuncVar(ADDS2B12,ch,12, 2)
print('*')
tfuncVar(ADDS3B12,ch,12, 3)
print('******')
ch="0123456789ABC"
tfuncVar(ADDS1B13,ch,13, 1)
print('*')
tfuncVar(ADDS2B13,ch,13, 2)
print('*')
tfuncVar(ADDS3B13,ch,13, 3)
print('******')
ch="0123456789ABCD"
tfuncVar(ADDS1B14,ch,14, 1)
print('*')
tfuncVar(ADDS2B14,ch,14, 2)
print('*')
tfuncVar(ADDS3B14,ch,14, 3)
print('******')
ch="0123456789ABCDE"
tfuncVar(ADDS1B15,ch,15, 1)
print('*')
tfuncVar(ADDS2B15,ch,15, 2)
print('*')
tfuncVar(ADDS3B15,ch,15, 3)
print('******')
ch="0123456789ABCDEF"
tfuncVar(ADDS1B16,ch,16, 1)
print('*')
tfuncVar(ADDS2B16,ch,16, 2)
print('*')
tfuncVar(ADDS3B16,ch,16, 3)
print('***ERREUR***')
tfuncVar(ADDS2B10,ch,10, 1)
