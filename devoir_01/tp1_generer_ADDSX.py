"fonction genere le code de ADDS1.py"
def genereADDS1():
    dex1 = open ("ADDS1.py",'w')    #creation du fichier
    dex1.write("def ADDS1 (a,b):\n")
    for i in range (0,10):          #change a
        for j in range (0,10):      #change b
            print ('    if(a==\''+str(i)+'\' and  b==\''+str(j)+'\'):', file = dex1)    #creation du texte
            print ("        return(\'"+str((i+j)%10)+"\',\'"+str((i+j)//10)+"\')",file = dex1)
    print ("    raise ValueError(\'unknown digits in ADDS1\')", file = dex1)
    dex1.close()    # fermeture du fichier

"fonction genere le code de ADDS1Bx.py"    
def genereADDS1X():
    for base in range(2,17):
        name = "ADDS1B"+str(base)+".py"     #creation du num de fichier
        var1 = open (name,'w')              #creation du fichier
        print ('def ADDS1B'+str(base)+'(a,b):',file = var1)
        ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']#les chiffres de base
        for a in range (0,base):#change valeur de 'a'
            for b in range (0,base):#change valeur de 'b'
                print ('    if(a==\''+ecvil[a]+'\' and  b==\''+ecvil[b]+'\'):', file = var1)
                print ("        return(\'"+ecvil[(a+b)%base]+"\',\'"+ecvil[(a+b)//base]+"\')",file = var1)
        print ("    raise ValueError(\'unknown digits in ADDS1B"+str(base)+"\')", file = var1)
        var1.close()    #fermeture du fichier
        
"fonction genere le code de ADDS2.py"
def genereADDS2():
    dex2 = open ("ADDS2.py",'w')        #creation du fichier
    dex2.write("def ADDS2 (a,b):\n")
    space = '    '
    for i in range (0,10):          #change valeur de 'a'
        print (space+'if(a==\''+str(i)+'\'):', file = dex2)
        for j in range(0,10):           #change valeur de 'b'
            print (2*space+'if(b==\''+str(j)+'\'):', file = dex2)
            print (3*space+"return(\'"+str((i+j)%10)+"\',\'"+str((i+j)//10)+"\')",file = dex2)
    print ("    raise ValueError(\'unknown digits in ADDS2\')", file = dex2)
    dex2.close()            #fermeture du fichier
    
"fonction genere le code de ADDS2Bx.py"
def genereADDS2X():
    for base in range(2,17):
        name = "ADDS2B"+str(base)+".py" #creation du num de fichier
        var2 = open (name,'w')      #creation du fichier
        print ('def ADDS2B'+str(base)+'(a,b):',file = var2)
        ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']       #les chiffres de base
        space = '    '
        for i in range (0,base): #change valeur de 'a'
            print (space+'if(a==\''+str(ecvil[i])+'\'):', file = var2)#genere la ligne du code
            for j in range(0,base): #change valeur de 'b'
                print (2*space+'if(b==\''+str(ecvil[j])+'\'):', file = var2) #genere la ligne du code
                print (3*space+"return(\'"+str(ecvil[(i+j)%base])+"\',\'"+str(ecvil[(i+j)//base])+"\')",file = var2)
        print ("    raise ValueError(\'unknown digits in ADDS2B"+str(base)+"\')", file = var2)
        var2.close()        #fermeture du fichier

"fonction genere le code de ADDS3.py"
def genereADDS3():
    dex3 = open ("ADDS3.py",'w')#creation du fichier
    dex3.write("def ADDS3 (a,b):\n")
    space = '    '
    n = 1   #quantite de space
    base = 10
    aL, bL = 0,0  # pointeur gauche pour a et b
    aR, bR = base-1, base-1     # pointeur droit pour a et b
    recura (base,n,aL,bL,aR,bR,dex3)
    dex3.close()#fermeture du fichier
    
def recura(base, n, aL, bL, aR, bR, fl):#genere la ligne du code pour a
    space = '    '
    aTemp = (aR+aL)//2 #la moitié
    if aTemp == aL:#pointeur gauche est égale à la moitié
        aText1 = str(n*space)+"if (a <= \'"+str(aL)+"\'):"
        print(aText1,file=fl)
        recurb (base,n+1,aL,fl, bR, bL)
        aText2 = str(n*space)+"else:"
        print(aText2,file=fl)
        recurb (base,n+1,aL+1,fl, bR, bL)
        return 2
    else:#pointeur gauche n'est pas égale à la moitié
        if aTemp+2 <= aR:#dans la partie droite plus  que deux nombres
            aLl = aL        #définition des pointeurs pour la partie gauche
            aRl = aTemp
            print(str(n*space)+'if (a <= \''+str(aRl)+'\'):',file=fl)
            recura (base,n+1,aLl,bL,aRl,bR,fl) #appel de recura avec les nouveux paramètres
            if aRl!=(base-1)//2: #pointeur droit de la partie gauche  n'est pas égale à la moitié
                aText3 = str((n+1)*space)+'else:'
                print(aText3,file=fl)
                recurb (base,n+2,aRl,fl, bR, bL)#appel de recurb
                print (str(n*space)+'else:',file = fl)
                n=n+1
            else:#pointeur droit de la partie gauche  est égale à la moitié
                print(str(n*space)+'else:',file=fl)
            aLr = aTemp+1#définition des pointeurs pour la partie droite
            aRr = aR
            if aRr-aLr>3:# dans la partie droite est plus que 3 nombres
                print(str((n+1)*space)+'if (a <= \''+str(aRr)+'\'):',file=fl)
                recura (base,n+2,aLr,bL, aRr, bR,fl)#appel de recura avec les nouveux paramètres
            else:
                recura (base,n,aLr,bL,aRr,bR,fl)#appel de recura avec les nouveux paramètres
        else:#dans la partie droite est moins  que deux nombres
            aL = aL
            aR = aTemp
            print(str(n*space)+'if (a <= \''+str(aR)+'\'):',file=fl)
            recura (base,n+1,aL,bL,aR,bR,fl)#appel de recura avec les nouveux paramètres

def recurb(base,n,a,fl, R, L):#genere la ligne du code pour b
    space = '    '
    Rtemp = (R+L)//2#la moitié
    if Rtemp == L:#pointeur gauche est égale à la moitié
        text = str(n*space)+"if (b <= \'"+str(Rtemp)+"\'):\n"+str((n+1)*space)+"return (\'"+str((Rtemp+a)%10)+"\',\'"+str((Rtemp+a)//10)+"\')\n"+str(n*space)+"else:\n"+str((n+1)*space)+"return (\'"+str((Rtemp+1+a)%10)+"\',\'"+str((Rtemp+1+a)//10)+"\')"
        print(text,file=fl)
        return 2
    else:#pointeur gauche n'est pas égale à la moitié
        if Rtemp+2 <= R:#dans la partie droite plus  que deux nombres
            Ll = L      #définition des pointeurs pour la partie gauche
            Rl = Rtemp
            print(str(n*space)+'if (b <= \''+str(Rl)+'\'):',file=fl)
            recurb (base,n+1,a,fl,Rl,Ll)#appel de recurb avec les nouveux paramètres
            if Rl!=(base-1)//2:    # pointeur droit de la partie gauche  n'est pas égale à la moitié
                text1 = str((n+1)*space)+'else:'+'\n'+str((n+2)*space)+'return (\''+str((Rtemp+a)%10)+"\',\'"+str((Rtemp+a)//10)+'\')\n'+str(n*space)+'else:'
                print(text1,file=fl)
                n=n+1
            else:
                print(str(n*space)+'else:',file=fl)
            Lr = Rtemp+1        #définition des pointeurs pour la partie droite
            Rr = R
            if Rr-Lr>3:# dans la partie droite est plus que 3 nombres
                print(str((n+1)*space)+'if (b <= \''+str(Rr)+'\'):',file=fl)
                recurb (base,n+2,a,fl,Rr,Lr)#appel de recurb avec les nouveux paramètres
            else:
                recurb (base,n,a,fl,Rr,Lr)#appel de recurb avec les nouveux paramètres
        else:#dans la partie droite est moins  que deux nombres
            L = L       #redéfinition des pointeurs
            R = Rtemp
            print(str(n*space)+'if (b <= \''+str(R)+'\'):',file=fl)
            recurb (base,n+1,a,fl,R,L)#appel de recurb avec les nouveux paramètres
            
"fonction genere le code de ADDS3Bx.py"
def genereADDS3X():
    for base in range(2,17):
        name = "ADDS3B"+str(base)+".py"#creation du num de fichier
        var3 = open (name,'w')#creation du fichier
        print ('def ADDS3B'+str(base)+'(a,b):',file = var3)
        space = '    '
        n = 1   #nombre de l'espace
        aL, bL = 0,0 # pointeur gauche pour a et b
        aR, bR = base-1, base-1  # pointeur droit pour a et b
        recura (base,n,aL,bL,aR,bR,var3)
        print ("    raise ValueError(\'unknown digits in ADDS3B"+str(base)+"\')", file = var3)
        var3.close()    #fermeture du fichier
   
def recura(base, n, aL, bL, aR, bR, fl):
    ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']   #les chiffres de base
    space = '    '
    aTemp = (aR+aL)//2  #la moitié
    if aTemp == aL:#pointeur gauche est égale à la moitié
        aText1 = str(n*space)+"if (a <= \'"+str(ecvil[aL])+"\'):"
        print(aText1,file=fl)
        recurb (base,n+1,aL,fl, bR, bL)
        aText2 = str(n*space)+"else:"
        print(aText2,file=fl)
        recurb (base,n+1,aL+1,fl, bR, bL)
        return 2
    else:   #pointeur gauche n'est pas égale à la moitié
        if aTemp+2 <= aR:#dans la partie droite plus  que deux nombres
            aLl = aL    #définition des pointeurs pour la partie gauche
            aRl = aTemp
            print(str(n*space)+'if (a <= \''+str(ecvil[aRl])+'\'):',file=fl)
            recura (base,n+1,aLl,bL,aRl,bR,fl)  #appel de recura avec les nouveux paramètres
            if aRl!=(aR+aL)//2:#pointeur droit de la partie gauche  n'est pas égale à la moitié
                aText3 = str((n)*space)+'else:'
                print(aText3,file=fl)
                recurb (base,n+1,aRl,fl, bR, bL)    #appel de recurb
                print (str((n-1)*space)+'else:',file = fl)
            else:#pointeur droit de la partie gauche  est égale à la moitié
                print(str(n*space)+'else:',file=fl)
                n=n+1
            aLr = aTemp+1#définition des pointeurs pour la partie droite
            aRr = aR
            if aRr-aLr>2:# dans la partie droite est plus que 2 nombres
                print(str((n+1)*space)+'if (a <= \''+str(ecvil[aRr])+'\'):',file=fl)
                recura (base,n+2,aLr,bL, aRr, bR,fl)    #appel de recura avec les nouveux paramètres
            else:
                recura (base,n,aLr,bL,aRr,bR,fl)    #appel de recura avec les nouveux paramètres
        else:   #dans la partie droite est moins  que deux nombres
            aLe = aL
            aRe = aTemp
            print(str(n*space)+'if (a <= \''+str(ecvil[aRe])+'\'):',file=fl)
            recura (base,n+1,aLe,bL,aRe,bR,fl)  #appel de recura avec les nouveux paramètres
            if aR == aTemp+1:
                print(str(n*space)+"else:",file=fl)
                recurb (base,n+1,aR,fl,bR,bL)#appel de recurb
            

def recurb(base,n,a,fl, R, L):  #genere la ligne du code pour b
    ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']   #les chiffres de base
    space = '    '
    Rtemp = (R+L)//2    #la moitié
    if Rtemp == L:  #pointeur gauche est égale à la moitié
        text = str(n*space)+"if (b <= \'"+str(ecvil[Rtemp])+"\'):\n"+str((n+1)*space)+"return (\'"+str(ecvil[(Rtemp+a)%base])+"\',\'"+str(ecvil[(Rtemp+a)//base])+"\')\n"+str(n*space)+"else:\n"+str((n+1)*space)+"return (\'"+str(ecvil[(Rtemp+1+a)%base])+"\',\'"+str(ecvil[(Rtemp+1+a)//base])+"\')"
        print(text,file=fl)
        return 2
    else:    #pointeur gauche n'est pas égale à la moitié   
        if Rtemp+2 <= R:    #dans la partie droite plus  que deux nombres
            Ll = L          #définition des pointeurs pour la partie gauche
            Rl = Rtemp      
            Lr = Rtemp+1        #définition des pointeurs pour la partie droite
            Rr = R
            print(str(n*space)+'if (b <= \''+str(ecvil[Rl])+'\'):',file=fl)
            recurb (base,n+1,a,fl,Rl,Ll)    #appel de recurb avec les nouveux paramètres
            if Rl!=(R+L)//2:     # pointeur droit de la partie gauche  n'est pas égale à la moitié
                text1 = str((n+1)*space)+'else:'+'\n'+str((n+2)*space)+'return (\''+str(ecvil[(Rtemp+a)%base])+"\',\'"+str(ecvil[(Rtemp+a)//base])+'\')\n'+str(n*space)+'else:'
                print(text1,file=fl)
                n=n+1
            else: # pointeur droit de la partie gauche est égale à la moitié
                print(str(n*space)+'else:',file=fl)
                n=n+1
            if Rr-Lr>3:     # dans la partie droite est plus que 3 nombres
                print(str((n)*space)+'if (b <= \''+str(ecvil[Rr])+'\'):',file=fl)
                recurb (base,n+1,a,fl,Rr,Lr)        #appel de recurb avec les nouveux paramètres
            else:
                recurb (base,n,a,fl,Rr,Lr)          #appel de recurb avec les nouveux paramètres
        else:           #dans la partie droite est moins  que deux nombres
            Le = L        #redéfinition des pointeurs
            Re = Rtemp
            print(str(n*space)+'if (b <= \''+str(ecvil[Re])+'\'):',file=fl)
            recurb (base,n+1,a,fl,Re,Le)    #appel de recurb avec les nouveux paramètres
            if R == Rtemp+1:
                print( str(n*space)+"else:\n"+str((n+1)*space)+"return (\'"+str(ecvil[(Rtemp+1+a)%base])+"\',\'"+str(ecvil[(Rtemp+1+a)//base])+"\')",file = fl )
            

    
    

