def genereADDS2X():
    for base in range(2,17):
        name = "ADDS2B"+str(base)+".py"
        var2 = open (name,'w')
        print ('def ADDS2B'+str(base)+'(a,b):',file = var2)
        ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
        space = '    '
        for i in range (0,base): #change valeur de 'a'
            print (space+'if(a==\''+str(ecvil[i])+'\'):', file = var2)#genere la ligne du code
            for j in range(0,base): #change valeur de 'b'
                print (2*space+'if(b==\''+str(ecvil[j])+'\'):', file = var2) #genere la ligne du code
                print (3*space+"return(\'"+str(ecvil[(i+j)%base])+"\',\'"+str(ecvil[(i+j)//base])+"\')",file = var2)
        print ("    raise ValueError(\'unknown digits in ADDS2B"+str(base)+"\')", file = var2)
        var2.close()
    
genereADDS2X()