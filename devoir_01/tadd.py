from ADDS1 import ADDS1

def tadd(a,b,len,ADD):
    r = '0'
    c = ''
    for i in range(-1, -(len+1),-1):
        #print (a[i],b[i])
        (c_tmp,r_tmp) = ADD(a[i],b[i])
        (c_tmp,r) = ADD(r,c_tmp)
        r = r if r!='0' else r_tmp
        c = c_tmp + c
    if (r!='0'):
        c = r+c
    return c
    
#c = tadd('01234','ABCDE',5,ADDS1)
#print ('res = ',c)
