def ADDS2B3(a,b):
    if(a=='0'):
        if(b=='0'):
            return('0','0')
        if(b=='1'):
            return('1','0')
        if(b=='2'):
            return('2','0')
    if(a=='1'):
        if(b=='0'):
            return('1','0')
        if(b=='1'):
            return('2','0')
        if(b=='2'):
            return('0','1')
    if(a=='2'):
        if(b=='0'):
            return('2','0')
        if(b=='1'):
            return('0','1')
        if(b=='2'):
            return('1','1')
    raise ValueError('unknown digits in ADDS2B3')
