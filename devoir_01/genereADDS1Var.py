def genereADDS1X():
    for base in range(2,17):
        name = "ADDS1B"+str(base)+".py"
        var1 = open (name,'w')
        print ('def ADDS1B'+str(base)+'(a,b):',file = var1)
        ecvil = ['0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
        for a in range (0,base):#change valeur de 'a'
            for b in range (0,base):#change valeur de 'b'
                print ('    if(a==\''+ecvil[a]+'\' and  b==\''+ecvil[b]+'\'):', file = var1)
                print ("        return(\'"+ecvil[(a+b)%base]+"\',\'"+ecvil[(a+b)//base]+"\')",file = var1)
        print ("    raise ValueError(\'unknown digits in ADDS1B"+str(base)+"\')", file = var1)
        var1.close()
        
genereADDS1X()