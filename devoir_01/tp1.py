import math, random, timeit, os
from tp1_generer_ADDSX import *
from ADDS1 import ADDS1
from ADDS2 import ADDS2
from ADDS3 import ADDS3
from ADDS1B2 import ADDS1B2
from ADDS1B3 import ADDS1B3
from ADDS1B4 import ADDS1B4
from ADDS1B5 import ADDS1B5
from ADDS1B6 import ADDS1B6
from ADDS1B7 import ADDS1B7
from ADDS1B8 import ADDS1B8
from ADDS1B9 import ADDS1B9
from ADDS1B10 import ADDS1B10
from ADDS1B11 import ADDS1B11
from ADDS1B12 import ADDS1B12
from ADDS1B13 import ADDS1B13
from ADDS1B14 import ADDS1B14
from ADDS1B15 import ADDS1B15
from ADDS1B16 import ADDS1B16
#
from ADDS2B2 import ADDS2B2
from ADDS2B3 import ADDS2B3
from ADDS2B4 import ADDS2B4
from ADDS2B5 import ADDS2B5
from ADDS2B6 import ADDS2B6
from ADDS2B7 import ADDS2B7
from ADDS2B8 import ADDS2B8
from ADDS2B9 import ADDS2B9
from ADDS2B10 import ADDS2B10
from ADDS2B11 import ADDS2B11
from ADDS2B12 import ADDS2B12
from ADDS2B13 import ADDS2B13
from ADDS2B14 import ADDS2B14
from ADDS2B15 import ADDS2B15
from ADDS2B16 import ADDS2B16
#
from ADDS3B2 import ADDS3B2
from ADDS3B3 import ADDS3B3
from ADDS3B4 import ADDS3B4
from ADDS3B5 import ADDS3B5
from ADDS3B6 import ADDS3B6
from ADDS3B7 import ADDS3B7
from ADDS3B8 import ADDS3B8
from ADDS3B9 import ADDS3B9
from ADDS3B10 import ADDS3B10
from ADDS3B11 import ADDS3B11
from ADDS3B12 import ADDS3B12
from ADDS3B13 import ADDS3B13
from ADDS3B14 import ADDS3B14
from ADDS3B15 import ADDS3B15
from ADDS3B16 import ADDS3B16

def tadd( a, b, len, ADD ):
    r="0"
    c=""
    for i in range( -1, -(len+1), -1 ):
        (c_tmp,r_tmp)= ADD(a[i],b[i]) #addition des deux chiffres
        (c_tmp,r) = ADD(r,c_tmp) #ajout du reste de la somme precedente
        r = r if r != '0' else r_tmp #le reste ne peut apparaitre aux deux
                                        #additions en meme temps
        c = c_tmp + c #concatenation de string
    if(r!="0"): #le dernier reste de l'addition est ajoute
        c = r + c
    return c

def tfunc1():
    for a in "0123456789":
        for b in "0123456789":
            ADDS1(a,b)

def tfunc2():
    for a in "0123456789":
        for b in "0123456789":
            ADDS2(a,b)

def tfunc3():
    for a in "0123456789":
        for b in "0123456789":
            ADDS3(a,b)

def tfuncVar(ADD,base):
    tousChiffres="0123456789ABCDEF"
    chiffres = tousChiffres[0:base]
    for base in range (2,17):
        for a in chiffres:
            for b in chiffres:
                ADD(a,b)

for k in (1, 10, 100, 1000, 10000, 100000, 1000000) :
    a = str(random.getrandbits(k))
    b = str(random.getrandbits(k))
    l = min(len(a),len(b))
    timeit.timeit('tadd(a,b,l, ADDS1)', setup= "from __main__ import tadd, a, b, l,ADDS1", number=10 )
for k in (1, 10, 100, 1000, 10000, 100000, 1000000) :
    a = str(random.getrandbits(k))
    b = str(random.getrandbits(k))
    l = min(len(a),len(b))
    timeit.timeit('tadd(a,b,l, ADDS2)', setup= "from __main__ import tadd, a, b, l,ADDS2", number=10 )
for k in (1, 10, 100, 1000, 10000, 100000, 1000000) :
    a = str(random.getrandbits(k))
    b = str(random.getrandbits(k))
    l = min(len(a),len(b))
    timeit.timeit('tadd(a,b,l, ADDS3)', setup= "from __main__ import tadd, a, b, l,ADDS3", number=10 )
timeit.timeit( 'tfunc1()', setup= "from __main__ import tfunc1", number=10000 )
timeit.timeit( 'tfunc2()', setup= "from __main__ import tfunc2", number=10000 )
timeit.timeit( 'tfunc3()', setup= "from __main__ import tfunc3", number=10000 )
'''#base
timeit.timeit( 'tfuncVar(ADDS1B2,2)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B3,3)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B4,4)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B5,5)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B6,6)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B7,7)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B8,8)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B9,9)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B10,10)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B11,11)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B12,12)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B13,13)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B14,14)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B15,15)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS1B16,16)', setup= "from __main__ import tfuncVar", number=10000 )
#
timeit.timeit( 'tfuncVar(ADDS2B2,2)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B3,3)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B4,4)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B5,5)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B6,6)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B7,7)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B8,8)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B9,9)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B10,10)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B11,11)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B12,12)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B13,13)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B14,14)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B15,15)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS2B16,16)', setup= "from __main__ import tfuncVar", number=10000 )
#
timeit.timeit( 'tfuncVar(ADDS3B2,2)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B3,3)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B4,4)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B5,5)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B6,6)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B7,7)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B8,8)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B9,9)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B10,10)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B11,11)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B12,12)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B13,13)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B14,14)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B15,15)', setup= "from __main__ import tfuncVar", number=10000 )
timeit.timeit( 'tfuncVar(ADDS3B16,16)', setup= "from __main__ import tfuncVar", number=10000 )
'''


