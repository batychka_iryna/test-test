from ADDS2 import ADDS2
from ADDS3 import ADDS3
import timeit

def tfunc2():
    for a in "0123456789":
        for b in "0123456789":
            ADDS2(a,b)

def tfunc3():
    for a in "0123456789":
        for b in "0123456789":
            ADDS3(a,b)

